# Inara/JOSS Template

This is a template that builds JOSS pdf documents from Markdown inside the CI pipeline using Inara.

The demo paper is taken from here: [https://joss.readthedocs.io/en/latest/submitting.html#example-paper-and-bibliography](https://joss.readthedocs.io/en/latest/submitting.html#example-paper-and-bibliography).

More information about JOSS: [https://joss.theoj.org/](https://joss.theoj.org/).

More information about Inara: [https://github.com/openjournals/inara](https://github.com/openjournals/inara).
